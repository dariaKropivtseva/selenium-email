package SeleniumAT;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EmailTest {

    private static ChromeDriver driver;
    private static WebDriverWait waiter;

    @BeforeClass
    public static void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.i.ua/?_rand=1600871277");
        waiter = new WebDriverWait(driver, 1000);
        authorize();
    }

    private static void authorize() {
        driver.findElement(By.cssSelector("[name='login']")).sendKeys("Test.qa.at@i.ua");
        driver.findElement(By.cssSelector("[name='pass']")).sendKeys("2020QAAT");
        WebElement btn = driver.findElement(By.cssSelector("[value='Войти'"));
        btn.click();
    }

    @Test
    public void test_userNameExisted() {
        String actual = driver.findElement(By.className("user_name")).getText();
        String expected = "Daria";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void checkMessagesQuantityBefore() {
        WebElement container = waiter.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".list_underlined")));

        String actual = container.findElement(By.tagName("li")).findElement(By.tagName("a")).getText();
        String expected = "5\nВходящие";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void writeMessage() {
        WebElement btnMakeMessage = driver.findElement(By.cssSelector(".make_message"));
        btnMakeMessage.click();

        WebElement container = waiter.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".text_editor_browser")));
        WebElement textArea = container.findElement(By.cssSelector("textarea[name='body']"));

        WebElement fieldTo = driver.findElement(By.cssSelector(".to"));
        WebElement textAreaTo = fieldTo.findElement(By.cssSelector("textarea[name='to']"));

        WebElement fieldTheme = driver.findElement(By.cssSelector(".subj"));
        WebElement textAreaTheme = fieldTheme.findElement(By.cssSelector("input[name='subject']"));

        WebElement btnSend = driver.findElement(By.cssSelector("input[name='send']"));

        textArea.sendKeys("Hi Daria");
        textAreaTo.sendKeys("Test.qa.at@i.ua");
        textAreaTheme.sendKeys("xz");
        String actual = btnSend.getAttribute("value");
        btnSend.click();

        String expected = "Отправить";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void checkMessagesQuantityAfter() {
        waiter.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".list_underlined")));
        driver.navigate().refresh();
        driver.navigate().refresh();
        WebElement container = waiter.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".list_underlined")));
        WebElement btn = driver.findElement(By.cssSelector("body > div.body_container > div.Body.right_300.clear > div.Left > div > div > div.Left > div.block_gamma.folders > div.content.clear > ul > li.new > a"));
        btn.click();
        driver.navigate().refresh();
        String actual = container.findElement(By.tagName("li")).findElement(By.tagName("a")).getText();
        String expected = "6\nВходящие";
        Assert.assertEquals(expected, actual);
    }
}
